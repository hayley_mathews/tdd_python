from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import unittest

class NewVisitorTest(unittest.TestCase):
    def setUp(self):
        self.browser = webdriver.Firefox()
        self.browser.implicitly_wait(3)

    def tearDown(self):
        self.browser.quit()

    def check_for_row_in_list_table(self, row_text):
        table = self.browser.find_element_by_id('id_list_table')
        rows = table.find_elements_by_tag_name('tr')
        self.assertIn(row_text, [row.text for row in rows])


    def test_can_start_a_list_and_retrive_it_later(self):
        # felix has heard about yet another online to-do app
        # he goes to check it out
        self.browser.get('http://localhost:8000')

        # he notices that unsuprisingly the page title and
        # header title mention to-do lists
        self.assertIn('To-Do', self.browser.title)
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('To-Do', header_text)

        # he is invited to enter a to-do item right away
        # and because he has nothing better to do
        # he enters "create my own to-do list app"
        inputbox = self.browser.find_element_by_id('id_new_item')
        self.assertEqual(
            inputbox.get_attribute('placeholder'),
            'Enter a to-do item')
        inputbox.send_keys('create my own to-do list app')

        # when he hits enter the page updates, and now lists
        # "1. create my own to-do list app" as an item in a to-do list
        inputbox.send_keys(Keys.ENTER)
        self.check_for_row_in_list_table('1: create my own to-do list app')

        # theres is still a text box inviting him to add another item
        # he enters "??????????"
        inputbox = self.browser.find_element_by_id('id_new_item')
        inputbox.send_keys('??????????')
        inputbox.send_keys(Keys.ENTER)

        # the page updates again and shows both items
        table = self.browser.find_element_by_id('id_list_table')
        self.check_for_row_in_list_table('1: create my own to-do list app')
        self.check_for_row_in_list_table('2: ??????????')



        # felix wonders if the site will remember his list
        # he sees that the site has generated a unique url for him
        self.fail('Finish the test!')

        # he visits the url and sees his to do list still theres

        # he gets bored and leaves


if __name__ == '__main__':
    unittest.main(warnings='ignore')
